<?php

namespace Database\Seeders;

use App\Models\User;
use App\Models\Vehicle;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        // Create eight user with no vehicle
        User::factory(8)->create();

        // Create two user with three vehicle each
        $users = User::factory(2)->create();
        foreach ($users as $user) {
            Vehicle::factory(3)->create([
                'user_id' => $user->id,
            ]);
        }

        // Create five users with one vehicle each
        $users = User::factory(5)->create();
        foreach ($users as $user) {
            Vehicle::factory(1)->create([
                'user_id' => $user->id,
            ]);
        }

        // Create four vehicles without any owners
        Vehicle::factory(4)->create();

    }
}
