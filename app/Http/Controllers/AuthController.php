<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;

class AuthController extends Controller
{
    function TokensCreate(Request $request){
        $user = User::where('email', $request['email'])->first();

        if(!$user){
            return response()->json([
                'message' => 'Does not exist user with that email',
            ], 401);
        }

        $token = $user->createToken('auth_token')->plainTextToken;

        return response()->json([
            'message' => 'Token created succesfully',
            'acces_token' => $token,
            'token_type' => 'Bearer',
        ], 200);
    }
}
