<?php

namespace App\Http\Controllers;

use App\Models\Vehicle;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;


class VehicleController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(): JsonResponse
    {
        $vehicles   = Vehicle::all();
        $status     = 200;
        $message    = "The vehicles has been loaded successfully";
        return response()->json([
            "message"   => $message,
            "data"      => $vehicles,
        ], $status);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create(Request $request): JsonResponse
    {
        $vehicle = Vehicle::create([
            "brand"     => $request->brand,
            "model"     => $request->model,
            "year"      => $request->year,
            "price"     => $request->price,
        ]);
        $status = 200;
        $message = "The vehicle has been created";
        return response()->json([
            "message"   => $message,
            "data"      => $vehicle,
        ], $status);
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id): JsonResponse
    {
        $vehicle    = Vehicle::find($id);
        $message    = "The vehicle has been loaded";
        $status     = 200;
        return response()->json([
            "message"   => $message,
            "data"      => $vehicle,
        ], $status);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id): JsonResponse
    {
        $vehicle = Vehicle::find($id);
        $message = "There is nothing to edit";
        $status = 200;
        if(!$vehicle){
            $message = "The vehicle you are trying to edit does not exist";
            return response()->json([
                "message" => $message,
            ], $status);
        }
        if(isset($request->brand) && $request->brand != $vehicle->brand){
            $vehicle->brand = $request->brand;
            $vehicle->touch();
            $message = "The brand was edited.";
        }
        if(isset($request->model) && $request->model != $vehicle->model){
            $vehicle->model = $request->model;
            $vehicle->touch();
            if(str_contains($message, "nothing"))
                $message = "The model was edited";
            else
                $message = "Brand and model were edited";
        }
        if(isset($request->year) && $request->year != $vehicle->year){
            $vehicle->year = $request->year;
            $vehicle->touch();
            if(str_contains($message, "nothing"))
                $message = "The year was edited";
            else if(str_contains($message, 'Brand and model'))
                $message = "Brand, model and year were edited";
            else if(str_contains($message, 'The model'))
                $message = "Model and year were edited";
            else
                $message = "Brand and year were edited";
        }
        if(isset($request->price) && $request->price != $vehicle->price){
            $vehicle->price = $request->price;
            $vehicle->touch();
            if(str_contains($message, "nothing"))
                $message = "The price was edited";
            else if(str_contains($message, "Brand, model and year"))
                $message = "The brand, model, year and price where edited";
            else if(str_contains($message, "Model and year"))
                $message = "The model, year and price were edited";
            else if(str_contains($message, 'Brand and year'))
                $message = "The brand, year and price were edited";
            else if(str_contains($message, "The year"))
                $message = "The year and price were edited";
            else if(str_contains($message, "Brand and model"))
                $message = "The brand, model and price were edited";
            else if(str_contains($message, 'The model'))
                $message = "The model and price were edited";
        }
        $vehicle->save();
        return response()->json([
            "message"   => $message,
            "data"      => $vehicle,
        ], $status);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id): JsonResponse
    {
        $vehicle = Vehicle::find($id);
        $status  = 200;
        if(!$vehicle){
            $message    = "There is no vehicle to delete";
        }
        else{
            $vehicle = $vehicle->delete();
            $message    = "The vehicle has been deleted";
        }
        return response()->json([
            "message"   => $message,
            "data"      => $vehicle,
        ], $status);
    }
}
