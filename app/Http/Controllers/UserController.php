<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(): JsonResponse
    {
        $users = User::all();
        $status = 200;
        $message = "The users has been loaded successfully";
        return response()->json([
            "message"   => $message,
            "data"      => $users,
        ], $status);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create(Request $request): JsonResponse
    {
        $user = User::create([
            "name"      => $request->name,
            "surname"   => $request->surname,
            "email"     => $request->email,
        ]);
        $status = 200;
        $message = "The user has been created";
        return response()->json([
            "message"   => $message,
            "data"      => $user,
        ], $status);
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id): JsonResponse
    {
        $user = User::find($id);
        $message = "The user has been loaded";
        $status = 200;

        return response()->json([
            "message"   => $message,
            "data"      => $user,
        ], $status);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id): JsonResponse
    {
        $user = User::find($id);
        $message = "There is nothing to edit";
        $status = 200;
        if(!$user){
            $message = "The user you are trying to edit does not exist";
            return response()->json([
                "message"   => $message,
            ], $status);
        }
        if(isset($request->name) && $request->name != $user->name){
            $user->name = $request->name;
            $user->touch();
            $message = "The name was edited.";
        }
        if(isset($request->surname) && $request->surname != $user->surname){
            $user->surname = $request->surname;
            $user->touch();
            if(str_contains($message, "nothing"))
                $message = "The surname was edited";
            else
                $message = "Name and surname were edited";
        }
        if(isset($request->email) && $request->email != $user->email){
            $user->email = $request->email;
            $user->touch();
            if(str_contains($message, "nothing"))
                $message = "The email was edited";
            else if(str_contains($message, 'Name and surname'))
                $message = "Name, surname and email were edited";
            else if(str_contains($message, 'The surname'))
                $message = "Surname and email were edited";
            else
                $message = "Name and email were edited";
        }
        $user->save();
        return response()->json([
            "message"   => $message,
            "data"      => $user,
        ], $status);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id): JsonResponse
    {
        $user = User::find($id);
        $status     = 200;
        if(!$user){
            $message    = "There is no user to delete";
        }
        else{
            $user = $user->delete();
            $message    = "The user has been deleted";
        }
        return response()->json([
            "message"   => $message,
            "data"      => $user,
        ], $status);
    }
}
