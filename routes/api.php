<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\VehicleController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::post('/tokens/create', [AuthController::class, 'TokensCreate']);

Route::group(['middleware' => ['auth:sanctum']], function (){
    // users routes
    Route::get('/users', [UserController::class, 'index']);
    Route::get('/users/{id}', [UserController::class, 'show']);
    Route::put('/users/{id}', [UserController::class, 'update']);
    Route::post('/users/create', [UserController::class, 'create']);
    Route::delete('/users/{id}', [UserController::class, 'destroy']);
    // vehicle routes
    Route::get('/vehicles', [VehicleController::class, 'index']);
    Route::get('/vehicles/{id}', [VehicleController::class, 'show']);
    Route::put('/vehicles/{id}', [VehicleController::class, 'update']);
    Route::post('/vehicles/create', [VehicleController::class, 'create']);
    Route::delete('/vehicles/{id}', [VehicleController::class, 'destroy']);
});
